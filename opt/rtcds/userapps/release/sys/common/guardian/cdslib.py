#!/usr/bin/env python3
from __future__ import print_function
import os
import re
import time
import configparser
import pexpect
from ezca import EzcaConnectError

##########

IFO = os.getenv('IFO')
SITE = os.getenv('SITE')
if not IFO:
    raise ImportError("could not find IFO environment variable.")
if not SITE:
    raise ImportError("could not find SITE environment variable.")

# Since cdslib is used in both guardian and outside of guardian,
# we need to do this awkward importing dance to get the site defined
# models to exclude, and maybe other info in this file later.
try:
    from models_exclude_lib import MODELS_EXCLUDE
except ModuleNotFoundError:
    import sys
    sys.path.append('{}/sys/{}/guardian'.format(os.getenv('USERAPPS_DIR'), IFO.lower()))
    from models_exclude_lib import MODELS_EXCLUDE

##########

RTCDS = '/opt/rtcds/{site}/{ifo}'.format(site=SITE.lower(), ifo=IFO.lower())
#if IFO == 'H1':
#    DAQ = 'h1dc0'
#elif IFO == 'L1':
#    DAQ = 'l1daqdc0'

RUNNING_DIR = '{}/daq0/running/'.format(RTCDS)

FEC_DCUID_RE = re.compile('{IFO}:FEC-([^_]*)_CPU_METER.HIGH'.format(IFO=IFO))
MODEL_RE = re.compile('{}(.*)'.format(IFO.lower()))

SDF_TABLE_MAP = {
    'both':  1,
    'table': 2,
    'epics': 4,
    'edb':   4,
    'monitored': 65540,
}

##########

def get_name_from_pseudo_dcuid(dcuid):
    """Since the autoBurt.req's are created the same time as the model,
    look through the models in the target area with names that end in
    sdf to find a DCUID.
    """
    # This also covers the case of no name and no dcuid initialized
    if dcuid < 1024:
        raise ValueError('pseudo-dcuids must be greater than 1023')
    # Make a list of all of the possible pseudo-FEs by looking through
    # the target dir and then filtering out the ones that end in sdf
    target_dir_files = [filenames for filenames in os.listdir('{}/target/'.format(RTCDS))]
    pseudo_mod_poss = [fm for fm in target_dir_files if re.match('{}.*sdf'.format(IFO.lower()), str(fm))]
    # Look into each possibilities autoburt.req and see if it has a DCUID
    # that matches the input
    for model in pseudo_mod_poss:
        with open('{rtcds}/target/{model}/{model}epics/autoBurt.req'.format(rtcds=RTCDS, model=model)) as f:
            pattern = '{IFO}:FEC-(....)_CPU_METER.HIGH'.format(IFO=IFO)
            for chan in f:
                if re.match(pattern, chan):
                    dcuid_infile = int(re.search('-([0-9]*)_', chan).group(1))
                    if dcuid_infile == dcuid:
                        return re.match('{}(.*)sdf'.format(IFO.lower()), model).group(1)
                    else:
                        break
    # If it reached this point then there isnt a model matching the dcuid
    raise ValueError('dcuid: {}, does not match a model in the target'.format(dcuid))


def get_pseudo_dcuid_from_name(fullmodname):
    """Look through the target area for the full model name
    then through its autoBurt.req to find the DCUID in a
    FEC channel.
    """
    modname_full = '{}{}sdf'.format(IFO.lower(), fullmodname)
    pattern = '{IFO}:FEC-(....)_CPU_METER.HIGH'.format(IFO=IFO)
    try:
        with open('{rtcds}/target/{model}/{model}epics/autoBurt.req'.format(rtcds=RTCDS, model=modname_full)) as f:
            for chan in f:
                if re.match(pattern, chan):
                    return int(re.search('-([0-9]*)_', chan).group(1))
    except FileNotFoundError:
        raise FileNotFoundError('No model "{}" ({}) found in target area'.format(full_modname, modname_full))


def get_pseudo_frontends():
    """Search through all of the models autoBurt.req in the
    target area for a DCUID that is four digits.
    """
    # Make a list of all of the possible pseudo-FEs by looking through
    # the target dir and then filtering out the ones that end in sdf
    target_dir_files = [filenames for filenames in os.listdir('{}/target/'.format(RTCDS))]
    pseudo_mod_poss = [fm for fm in target_dir_files if re.match('{}.*sdf'.format(IFO.lower()), str(fm))]
    # Look into each possibilities autoburt.req for a 4 digit dcuid
    dcuid_re = re.compile('{IFO}:FEC-(....)_CPU_METER.HIGH'.format(IFO=IFO))
    model_re = re.compile('{}(.*)sdf'.format(IFO.lower()))
    for model in pseudo_mod_poss:
        with open('{rtcds}/target/{model}/{model}epics/autoBurt.req'.format(rtcds=RTCDS, model=model)) as f:
            for chan in f:
                dcuid = dcuid_re.match(chan)
                if dcuid:
                    name = model_re.match(model).group(1)
                    dcuid = int(dcuid.group(1))
                    if name in MODELS_EXCLUDE:
                        continue
                    yield (name, dcuid)


####

def _get_running_models_ini():
    """Generator over model (name, inifile) tuples from ini files in daq0 running dir.

    """
    re_name = re.compile('{IFO}(.*).ini$'.format(IFO=IFO))
    for f in os.listdir(RUNNING_DIR):
        if f[-3:] == 'ini':
            name = re_name.match(f).group(1).lower()
            if name in MODELS_EXCLUDE:
                continue
            yield name, '{}{}'.format(RUNNING_DIR, f)


def _get_running_models_dcuid(pseudo=True):
    """Generator that yields model (name, dcuid) tuple from running
    ini files.

    If pseudo=True, find the Beckhoff frontends via their autoburts
    """
    #ini_list = []
    #for f in os.listdir(RUNNING_DIR):
    #    if f[-3:] == 'ini':
    #        ini_list.append(f)
    
    re_fec = re.compile('\[.*:FEC-(.*)_CPU_METER\]')
    for (name, ini) in _get_running_models_ini():
        with open(ini) as f:
            for line in f:
                g = re_fec.search(line)
                if g:
                    dcuid = int(g.group(1))
                    yield name, dcuid
    if pseudo:
        for (name, dcuid) in get_pseudo_frontends():
            yield name, dcuid
    

def _get_dcuid_ezca(name):
    """Return DCUID for given model name by channel access.

    """
    if name in MODELS_EXCLUDE:
        raise ValueError('Model \'{}\' excluded'.format(name))
    # Handle models names with or without ifo prefix
    if name[:2] != IFO.lower():
        name = '{}{}'.format(IFO.lower(), name)
    sys = name[2:5].upper()
    rest = name[5:].upper()
    if rest:
        prefix = '%s-%s_' % (sys, rest)
    else:
        prefix = '%s-' % (sys)
    chan = '%sDCU_ID' % (prefix)
    return int(ezca[chan])

#####

def _get_open_chnums_dcuid_ezca(dcuid):
    """Look through the GDS test point lists via ezca.
    Return a list of open excitations and test point chnums
    """
    gds_tp_max = 32
    tp_total = int(ezca['FEC-{}_TP_CNT'.format(dcuid)])
    # First see if there are any test points to save time
    if tp_total == 0:
        return []
    # There is only 32 slots for tps in gdsmon, so we have to run
    # the diag prog if over that. We want to avoid this for Guardian use
    elif tp_total > gds_tp_max:
        diag = pexpect.spawn('diag -l')
        diag.expect('diag> ')
        diag.sendline('tp show {}'.format(dcuid))
        diag.expect('diag> ')
        d_out = diag.before
        d_output = d_out.decode('unicode_escape')

        # Make list of lines
        d_output = d_output.split('\r\n')
        # Remove empty
        dout = []
        for line in d_output:
            if line:
                dout.append(line)
        # Sort for EXs and TPs
        for entry in dout:
            if 'EX:' in entry:
                lsc_ex_raw = entry
            elif 'TP:' in entry:
                lsc_tp_raw = entry

        regex = re.compile('[1-9][0-9]*')
        lsc_ex = [int(chnum) for chnum in lsc_ex_raw.split() if regex.match(chnum)]
        lsc_tp = [int(chnum) for chnum in lsc_tp_raw.split() if regex.match(chnum)]

        return lsc_ex + lsc_tp

    # This should be the normal way to get tps if they exist
    else:
        gdsmon_chan = 'FEC-{}_GDS_MON_{}'
        tpnums = []
        # Unfortunately we need to scan through all of these
        # because there could be empty slots
        for i in range(32):
            tpnums.append(int(ezca[gdsmon_chan.format(dcuid, i)]))
        # Filter out empty slots
        tpnums = [chnum for chnum in tpnums if tpnums != 0]

        return tpnums


def _get_channels_chnums(model, chnums):
    """Get the channel name(s) for a model from a list of exc/tp chnums.

    """
    model_full = '{}{}'.format(IFO.lower(), model)
    # Convert chnums to int if not already
    chnums = [int(chnum) for chnum in chnums]
    # Go into target/gds/param/ and look through the .par file to get the chnnum and name
    # Use configparser for easy ini/par file parsing
    parfile = configparser.ConfigParser()
    parfile.read('{rtcds}/target/gds/param/tpchn_{model}.par'.format(rtcds=RTCDS, model=model_full))
    chan_names = []
    for channel in parfile.sections():
        if int(parfile[channel]['chnnum']) in chnums:
            chan_names.append(channel)

    return chan_names


def _get_open_tps(dcuid, model):
    open_chnums = _get_open_chnums_dcuid_ezca(dcuid)
    chans = _get_channels_chnums(model, open_chnums)
    tp_chans = [chan for chan in chans if not chan.endswith('_EXC')]
    return tp_chans


def _get_open_excitations(dcuid, model):
    open_chnums  = _get_open_chnums_dcuid_ezca(dcuid)
    chans = _get_channels_chnums(model, open_chnums)
    exc_chans = [chan for chan in chans if chan.endswith('_EXC')]
    return exc_chans

##########

class CDSFrontEnd(object):
    """Interface to CDS Front End model.

    """
    def __init__(self, name, dcuid):
        """Initalize with the model name and dcuid.

        Name should be without leading <ifo> prefix:

        e.g. 'lsc', 'susetmx', etc.

        """
        self.__ifo = IFO.lower()
        self.__name = name.lower()
        self.__DCUID = int(dcuid)

    @classmethod
    def find(cls, uid):
        """Retrieve CDSFrontEndModel by name or dcuid."""
        name = None
        dcuid = None
        try:
            dcuid = int(uid)
        except ValueError:
            try:
                name = uid.lower()
            except AttributeError:
                raise ValueError("could not parse model: {}".format(uid))
        if name:
            try:
                dcuid = _get_dcuid_ezca(name)
            # The pseudo models dont have the above channel, so try this next
            except EzcaConnectError:   ### THis doesn't work, look into it
            # FIXME: Overly caught exception, 
            #except:
                dcuid = get_pseudo_dcuid_from_name(name)
            return cls(name, dcuid)
        # as last resort, find model/dcuid by scanning all available models
        for n, d in _get_running_models_dcuid():
            if n == name or d == dcuid:
                name = n
                dcuid = d
                return cls(name, dcuid)
        raise ValueError("could not find model: {}".format(uid))

    @property
    def name(self):
        """Model abbreviated name."""
        return self.__name

    @property
    def fullname(self):
        """Model full name."""
        return '%s%s' % (self.__ifo, self.__name)

    @property
    def DCUID(self):
        """Model DCUID."""
        return self.__DCUID
    FEC = DCUID
    dcuid = DCUID
    fec = DCUID

    @property
    def pseudo(self):
        return self.dcuid > 1023

    def __str__(self):
        pseudo_flag = ''
        if self.pseudo:
            pseudo_flag = 'PSEUDO'
        return "<{} '{}' (dcuid: {}) {}>".format(
            self.__class__.__name__,
            self.name,
            self.dcuid,
            pseudo_flag,
        )

    def __fec_chan(self, chan):
        return 'FEC-%d_%s' % (self.dcuid, chan)

    def __getitem__(self, chan):
        """Get FEC channel value."""
        return ezca[self.__fec_chan(chan)]

    def __setitem__(self, chan, value, wait=True):
        """Set FEC channel value"""
        ezca.write(self.__fec_chan(chan), value, wait=wait)
    put = __setitem__

    ##########
    # SDF

    @property
    def SDF_DIFF_CNT(self):
        return int(self['SDF_DIFF_CNT'])

    @property
    def SDF_TABLE_LOCK(self):
        """0/False if there are no pending changes,
        1/True if there are pending changes.
        Return bool
        """
        return bool(self['SDF_TABLE_LOCK'])

    def sdf_get_request(self):
        """Get requested SDF snap file."""
        return self['SDF_NAME']

    def sdf_get_loaded_table(self):
        """Get SDF snap currently loaded to TABLE."""
        return self['SDF_LOADED']

    def sdf_get_loaded_edb(self):
        """Get SDF snap currently loaded to EPICS db."""
        return self['SDF_LOADED_EDB']

    def sdf_load(self, snap, loadto='table', confirm=True):
        """Load SDF snap file to table or EPICS db.
        
        The `loadto` option can be used to specify how the snap file
        is loaded.  The options are:
        
          'table': load into SDF table only (default)
          'epics': load into EPICS db only
          'both': load into both SDF table and EPICS db
          'monitored': load into EPICS only the monitored channels and their values.

        `confirm` True will check that the tables are actually loaded in the end.

        """
        assert loadto in SDF_TABLE_MAP.keys(), "Invalid `loadto` value (valid opts: %s)." % SDF_TABLE_MAP.keys()

        s = loadto
        if loadto == 'both':
            s = 'table+edb'
        log("SDF LOAD: %s %s: '%s'" % (self.name, s, snap))

        self.put('SDF_NAME', snap)
        time.sleep(0.1)
        self.put('SDF_RELOAD', SDF_TABLE_MAP[loadto])

        # confirm that the file has been loaded
        if confirm:
            if loadto in ['edb', 'both', 'monitored']:
                while self.sdf_get_loaded_edb() != snap:
                    time.sleep(0.01)
            if loadto in ['table', 'both']:
                while self.sdf_get_loaded_table() != snap:
                    time.sleep(0.01)

    ##########
    # STATE_WORD

    @property
    def STATE_WORD(self):
        return int(self['STATE_WORD'])

    @property
    def excitation_active(self):
        word = self.STATE_WORD
        return bool(word&(1<<8))

    @property
    def open_tps(self):
        return _get_open_tps(self.DCUID, self.name)

    @property
    def open_excs(self):
        return _get_open_excitations(self.DCUID, self.name)
    
    
##########

class CDSFrontEnds:
    """Interface to all CDS Front End models

    Includes all front-end models, including all "pseudo" front-end SDF IOCs

    """
    def __init__(self):
        self.models = {name:CDSFrontEnd(name, dcuid) for name, dcuid in _get_running_models_dcuid()}

    def __getitem__(self, model):
        return self.models[model]

    def __contains__(self, model):
        return model in self.models

    def keys(self):
        return self.models.keys()

    def values(self):
        return self.models.values()

    def items(self):
        return self.models.items()

    def sdf_load(self, snap, loadto='both', models=None, exclude=None, fast=False):
        """Load specified SDF snap file in all models

        See CDSFrontEnd.sdf_load() for meanings of the `loadto` argument.

        `models` is a list of models for which to load, otherwise all
        are loaded.  `exclude` is a list of models to exclude from
        loading.

        If `fast` is True the loading is attempted as fast as
        possible, by setting table and loading in separate passes.
        WARNING: this is potentially less robust.

        """
        if models is None:
            models = self.models.keys()
        if exclude is not None:
            models = [m for m in models if m not in exclude]

        # convert models to list of names to list of CDSFrontEnd
        # objects
        models = [model for name, model in self.models.items() if name in models]

        if fast:
            # set file
            for model in models:
                model.put('SDF_NAME', snap, wait=False)

            # load
            for model in models:
                model.put('SDF_RELOAD', SDF_TABLE_MAP[loadto], wait=False)

        else:
            for model in models:
                model.sdf_load(snap, loadto=loadto)

    @property
    def all_open_tps(self):
        models = [model for name, model in self.models.items() if not model.pseudo]
        open_tps = []
        for model in models:
            try:
                open_tps.append(_get_open_tps(model.dcuid, model.name))
            except ValueError:
                continue
        # Remove empty strings. Left with list of lists
        reduced = filter(None, open_tps) 
        return [ch for ch_list in reduced for ch in ch_list]

    @property
    def all_open_excs(self):
        models = [model for name, model in self.models.items() if not model.pseudo]
        open_excs = []
        for model in models:
            try:
                open_excs.append(_get_open_excitations(model.dcuid, model.name))
            except ValueError:
                continue
        reduced = filter(None, open_excs) 
        return [ch for ch_list in reduced for ch in ch_list]


##################################################
# Merged functions
##########

# History - there was a time where a cdslib.py and a cdslib2.py existed.
# They stayed seperate for so long that to merge the two versions
# meant that some functions needed to be preserved for outside support.
# They are awkward and a bit hacky because of this.

##########
# FE tools


def get_active_model_ini():
    """Iterator over active front end model (name, inifile) tuples.

    """
    return _get_running_models_ini()


def get_active_model_dcuid():
    """Iterator over active (model, dcuid) tuples.

    """
    return _get_running_models_dcuid(pseudo=False)


def ezca_get_dcuid(modname):
    """Return DCUID number for a given full model name.

    """
    return _get_dcuid_ezca(modname)

##########
# Model generators

def get_active_models():
    models = CDSFrontEnds().items()
    for model, mod_obj in models:
        if not mod_obj.pseudo:
            yield mod_obj

def get_all_models():
    models = CDSFrontEnds().items()
    for model, mod_obj in models:
        yield mod_obj

##################################################

summary = "list front end models and DCUIDs"

def main():
    parser = argparse.ArgumentParser(description=summary,
                                     formatter_class=argparse.RawDescriptionHelpFormatter)

    parser.add_argument('model', nargs='?', help='Give the model name or dcuid # and it will return the compliment.')
    parser.add_argument('-p', '--printmodels', action='store_true', help='Print the total list of the all models and their dcuids.')
    parser.add_argument('-pn', '--printmodelsnumbers', action='store_true', help='Print the total list of the all models sorted by their dcuids.')
    parser.add_argument('-t', '--testpoints', action='store_true',
                        help='Print open test points and excitations. If paired with a model/dcuid it only prints their tps.')

    args = parser.parse_args()

    def print_model(model):
        print('{} {}'.format(
            model.name,
            model.dcuid
        ))

    # Clean up the model argument if its there
    if args.model:
        try:
            mod = int(args.model)
        except ValueError:
            mod = args.model
        if isinstance(mod, str):
            if mod[:2] == os.getenv('ifo'):
                mod = mod[2:]
        
    if args.model and not args.testpoints and not args.printmodels:
        model = CDSFrontEnd.find(mod)
        print_model(model)

    if args.printmodels:
        fes = CDSFrontEnds()
        model_list = list(fes.items())
        model_list.sort()
        for name, model in model_list:
            print_model(model)

    if args.printmodelsnumbers:
        fes = CDSFrontEnds()
        nums_names = []
        for name, model in fes.items():
            nums_names.append((model.dcuid, model.name))
        nums_names.sort()
        for num, name in nums_names:
            print(num, name)
        
    elif args.model and args.testpoints:
        model = CDSFrontEnd.find(mod)
        print_model(model)
        print('\nOpen EXC channels:\n')
        for ch in model.open_excs:
            print(ch)
        print('\nOpen TP channels:\n')
        for ch in model.open_tps:
            print(ch)

    elif not args.model and args.testpoints:
        fes = CDSFrontEnds()
        print('Finding open test points. This may take a min...')
        tps = fes.all_open_tps
        excs = fes.all_open_excs
        print('\nOpen EXC channels:\n')
        for ch in excs:
            print(ch)
        print('\nOpen TP channels:\n')
        for ch in tps:
            print(ch)

            
if __name__ == '__main__':
    import argparse
    import os
    from ezca import Ezca
    ezca = Ezca()
    
    main()
