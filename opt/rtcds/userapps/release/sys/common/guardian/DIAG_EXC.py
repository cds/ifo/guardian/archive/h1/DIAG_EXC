from SYS_DIAG import *

import cdslib

##################################################

FEMODELS = list(cdslib.get_active_models())

EXCLUDE_LIST = []

@SYSDIAG.register_test
def EXC():
    """active excitations"""
    for cm in FEMODELS:
        if cm.name in EXCLUDE_LIST:
            continue
        if cm.name == 'calinj':
            excitations = cm.open_excs
            for exc in excitations:
                # These have the IFO in the channel, splice
                if not exc[3:] == 'CAL-INJ_CW_EXC':
                    yield f'{cm.name.upper()} ({cm.fullname}) excitation!'
        elif cm.excitation_active:
            name = cm.name.upper()
            name = '%s %s' % (name[:3], name[3:])
            model = cm.fullname
            yield '%s (%s) excitation!' % (name, model)

##################################################

if __name__ == '__main__':

    from ezca import Ezca
    Ezca().export()

    for m in EXC():
        print(m)
